'use strict';

document.addEventListener("DOMContentLoaded", function() {


    // Modal
    $(".btn-modal").fancybox({
        autoFocus: false,
    });

    $('.btn-file').on('click', function(e){
        e.preventDefault();
        $.fancybox.open({
            src  : '#file-upload',  
            type : 'inline',
            opts : {
                afterShow : function( instance, current ) {
                    console.info( 'done!' );
                }
            }
        });
    });

    // file Form
    $('.upload-box__form--input').on('change', function(e) {
        var str = $(this).val();

        if (str.lastIndexOf('\\')){
            var i = str.lastIndexOf('\\')+1;
        }
        else{
            var i = str.lastIndexOf('/')+1;
        }
        var filename = str.slice(i);
        console.log(filename);

        $('.btn-file').text(filename);

        $.fancybox.close({
            src  : '#file-upload',  
            opts : {
                afterShow : function( instance, current ) {
                    console.info( 'done!' );
                }
            }
        });

    });



    // SVG IE11 support
    svg4everybody();
    
    // Mask input
    $("input[name='phone']").mask("+7(999) 999-9999");

    // Mobile Nav Toggle
    $('.nav-toggle').on('click', function(e){
        e.preventDefault();
        $('body').toggleClass('nav-open');
    });

    // Services
    $(".services__item")
    .on('mouseenter', function() {
        $(this).find('.services__hover').slideToggle('fast');
    })
    .on('mouseleave', function(){
        $(this).find('.services__hover').slideToggle('fast');
    });

    // Service nav

    $(".service-menu__item")
    .on('mouseenter', function() {
        $(this).find('.service-menu__item--hover').slideToggle('fast');
    })
    .on('mouseleave', function(){
        $(this).find('.service-menu__item--hover').slideToggle('fast');
    });


    // Media Slider 
    let isMediaSlider = $('div').is('.media-slider'); 
    if (isMediaSlider) {
        var mediaSlider = new Swiper('.media-slider', {
            loop: true,
            pagination: {
              el: '.media-slider-pagination',
              clickable: true,
            },
            navigation: {
              nextEl: '.media-slider-next',
              prevEl: '.media-slider-prev',
            },
        })
    
        mediaSlider.on('slideChange', function () {
            let sld = mediaSlider.realIndex + 1;
            $('.content-media-counter').text('0' + sld);
        });
    }


    // 
    $('.accordion__header').on('click', function(e){
        e.preventDefault();
        let item = $($(this).closest('.accordion__item'));
        if (item.hasClass('active')) {
            item.removeClass('active');
            item.find('.accordion__content').slideUp('fast');
        }
        else {
            $('.accordion__item').removeClass('active');
            $('.accordion__content').slideUp('fast');
            item.addClass('active');
            item.find('.accordion__content').slideDown('fast');
        }
    });

    // sidenav
    $('.sidenav__header').on('click', function(e){
        e.preventDefault();            
        $('.sidenav').toggleClass('open');
    });

    var newsSlider = new Swiper('.news-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 16,
        navigation: {
          nextEl: '.news-next',
          prevEl: '.news-prev',
        },
        breakpoints: {
            678: {
              slidesPerView: 2,
              spaceBetween: 16,
            },
            1024: {
              slidesPerView: 3,
              spaceBetween: 16,
            },
        }
    })

    // Service block
    let isService = $('div').is('.service'); 
    if (isService) {

        let serviceMobileSlider = new Swiper('.service-mobile-nav', {
            loop: false,
            slidesPerView: 1,
            spaceBetween: 16,
            navigation: {
              nextEl: '.service-mobile-next',
              prevEl: '.service-mobile-prev',
            }
        })
    
        let serviceSlider = new Swiper('.service-slider', {
            loop: false,
            slidesPerView: 1,
            spaceBetween: 0,
            navigation: {
              nextEl: '.service-slider-next',
              prevEl: '.service-slider-prev',
            }
        })

        serviceMobileSlider.on('slideChange', function () {
            let sld = serviceMobileSlider.realIndex
            serviceSlider.slideTo(sld, 300);
        });
        
        serviceSlider.on('slideChange', function () {
            let sld = serviceSlider.realIndex;
            let sld_num = sld + 1;
            let sld_nav = '.service-nav-' + sld;
            serviceMobileSlider.slideTo(sld, 300);
            $('.service-slider-number').text('0' + sld_num);
            $('.service__nav--item').removeClass('active');
            $(sld_nav).addClass('active');

            console.log(sld_nav);
        });

        $('.service__nav--item').on('click', function(e){
            e.preventDefault();
            let slideNum = $(this).attr('data-target');
            $('.service__nav--item').removeClass('active');
            $(this).addClass('active');
            serviceSlider.slideTo(slideNum, 300);
        });
        
        // Rec Block 
        let isRec = $('div').is('.rec'); 
        if (isRec) {

            let recSlider = new Swiper('.rec-slider', {
                loop: true,
                slidesPerView: 1,
                spaceBetween: 0,
                navigation: {
                  nextEl: '.rec-next',
                  prevEl: '.rec-prev',
                }
            })

            recSlider.on('slideChange', function () {
                let sld = recSlider.realIndex + 1;
                $('.rec-number').text('0' + sld);
            });
        }   
    }

    // Super Block 
    let isSuper = $('div').is('.super'); 
    if (isSuper) {

        let superSlider = new Swiper('.super-slider', {
            loop: true,
            slidesPerView: 1,
            spaceBetween: 0,
            pagination: {
                el: '.super__pagination--wrap',
                clickable: true,
            },
            navigation: {
              nextEl: '.super-next',
              prevEl: '.super-prev',
            }
        })

        superSlider.on('slideChange', function () {
            let sld = superSlider.realIndex + 1;
            $('.super-number').text('0' + sld);
        });
    };

    // Rec Block 
    let isRec = $('div').is('.rec'); 
    if (isRec) {

        let recSlider = new Swiper('.rec-slider', {
            loop: true,
            slidesPerView: 1,
            spaceBetween: 0,
            navigation: {
              nextEl: '.rec-next',
              prevEl: '.rec-prev',
            }
        })

        recSlider.on('slideChange', function () {
            let sld = recSlider.realIndex + 1;
            $('.rec-number').text('0' + sld);
        });
    }  

    // Benefit Block 
    let isBenefit = $('div').is('.benefit'); 
    if (isBenefit) {

        let benefitSlider = new Swiper('.benefit-slider', {
            loop: true,
            slidesPerView: 1,
            spaceBetween: 10,
            pagination: {
                el: '.benefit__pagination--wrap',
                clickable: true,
            },
            navigation: {
              nextEl: '.benefit-next',
              prevEl: '.benefit-prev',
            },
            breakpoints: {
               1300: {
                  slidesPerView: 1,
                  spaceBetween: 30,
                },
            }
        })

        benefitSlider.on('slideChange', function () {
            let sld = benefitSlider.realIndex + 1;
            $('.benefit-number').text('0' + sld);
        });
    } 

    // Press Block 
    let isPress = $('div').is('.press-block'); 
    if (isPress) {
        
        let pressMobile = new Swiper('.press-slider-mobile', {
            loop: true,
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
              nextEl: '.press-next',
              prevEl: '.press-prev',
            }
        });
                
        let pressDesktop = new Swiper('.press-slider-desktop', {
            loop: true,
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
              nextEl: '.press-next',
              prevEl: '.press-prev',
            }
        });
    }
    
    // Filter
    $('.filter-item').on('click', function(e){
        e.preventDefault();
        $(this).closest('.sidenav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');
    });

    // job slider
    let jobSlider = new Swiper('.job-slider-main', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
          nextEl: '.job-slider__nav--next',
          prevEl: '.job-slider__nav--prev',
        }
    });
    
});
